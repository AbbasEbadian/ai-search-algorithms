// import libraries
#include <iostream>
#include <deque>
#include <string.h>

using namespace std;

// Node Class
class Node{
	public:
	string state;
	int depth;
	string path;
	int cost;
	Node(){}
	
	Node(string _state, int _depth, string dir){
		state = _state;
		depth = _depth;
		path = dir;
		cost = _depth;
	}
};


/*
	 Test cases
	
	 312645078
	 3 1 2
	 6 4 5
	 0 7 8
	 Solution : UU
	 
	 -----------------
	 
	 140352678
	 1 4 0
	 3 5 2
	 6 7 8
	 Solution : DLUL
	
	------------------
	
	 310452678
	 3 1 0 
	 4 5 2
	 6 7 8
	 Solution:  DLLU
	 
	 -----------------
	 
	 102347685
	 1 0 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 0 2 | 0 1 2
	 3 4 7 | 3 0 7 | 3 7 0 | 3 7 5 | 3 7 5 | 3 0 5 | 3 4 5 | 3 4 5
	 6 8 5 | 6 8 5 | 6 8 5 | 6 8 0 | 6 0 8 | 6 7 8 | 6 7 8 | 6 7 8
	 Solution:  DRDLUUL
	
	------------------
	
	125704368
	 1 2 5
	 7 0 4
	 3 6 8
	 Solution:  LDRURULL
 
 
 */




int main(){
	// define values 
	string start;
	cout << "start state: ";
	cin >> start;
	string GOAL =  "012345678";
	
	// fringe or queue
	deque <Node> Q;
	// root node
	Node root = Node(start, 0, "");
	
	int created_nodes= 1;
	Node current;
	
	// push first node in queue
	Q.push_back(root);
	cout << "Searching solution for " << start << endl;
	
	while(!Q.empty()){
		// get next element of queue
		current = Q.front();
		
		// if its goal break
		if(current.state == GOAL) break;
		
		// find location of 0 in current state as k
		int k;
		for (k=0; k<10; k++){
			if (current.state[k] == '0'){
				break;
			}
		}
		
		// move up state node
		if (k > 2){
			string up_state = current.state;
			swap(up_state[k], up_state[k-3]);
			Node up =  Node(up_state, (current.depth)+1, current.path+'U');
			Q.push_back(up);
			created_nodes++;
		}
		
		// move left state node
		if (k % 3 > 0){
			string left_state = current.state;
			swap(left_state[k], left_state[k-1]);
			Node left =  Node(left_state, (current.depth+1), current.path+'L');	
			Q.push_back(left);
			created_nodes++;
		}
		// move bottom state node
		if (k < 6){
			string bottom_state = current.state;
			swap(bottom_state[k], bottom_state[k+3]);
			Node bottom = Node(bottom_state, (current.depth)+1,current.path+ 'D');
			Q.push_back(bottom);
			created_nodes++;
		}
		// move right state node
		if (k % 3 < 2){
			string right_state = current.state;
			swap(right_state[k], right_state[k+1]);
			Node right =  Node(right_state, (current.depth)+1, current.path+'R');
			Q.push_back(right);
			created_nodes++;
		}
		
		system("CLS");
		cout << "Searching solution for " << start << endl;
		cout << "Current depth: " << current.depth <<endl;
		cout << "Generated nodes: " << created_nodes <<endl;
		
		// sort queue by cost ( g(n) )
		for (int i=0; i<Q.size()-1; i++){
			for (int j=0; j<Q.size()-i-1; j++){
				if (Q[j].cost > Q[j+1].cost){
					Node temp = Q[j];	
					Q[j] =  Q[j+1];
					Q[j+1] = temp;
				} 
			}
		}
		// remove the explored node from queue
		Q.pop_front();
	}
	
	// print the depth and path of GOAL
	cout << "Found in depth: " << current.depth << endl;
	cout << "PATH: " << current.path;
	return 0;	
}


