// import libraries
#include <iostream>
#include <deque>
#include <string.h>
#include <math.h>

using namespace std;
int get_h(string);

// Node Class
class Node{
public:
	string state;
	string path;
	int depth;
	int cost;
	Node(){	}
	
	
	Node(string _state, int _depth, string _path){
		state = _state;
		depth = _depth;
		path = _path;
		cost = _depth + get_h(_state);
	}

};

/*
 Test cases

 312645078
 3 1 2
 6 4 5
 0 7 8
 Solution : UU
 
 -----------------
 
 140352678
 1 4 0
 3 5 2
 6 7 8
 Solution : DLUL

------------------

 310452678
 3 1 0 
 4 5 2
 6 7 8
 Solution:  DLLU
 
 -----------------
 
 102347685
 1 0 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 4 2 | 1 0 2 | 0 1 2
 3 4 7 | 3 0 7 | 3 7 0 | 3 7 5 | 3 7 5 | 3 0 5 | 3 4 5 | 3 4 5
 6 8 5 | 6 8 5 | 6 8 5 | 6 8 0 | 6 0 8 | 6 7 8 | 6 7 8 | 6 7 8
 Solution:  DRDLUUL

------------------

125704368
 1 2 5
 7 0 4
 3 6 8
 Solution:  LDRURULL
 
 
 */



int main(){
	string start;
	cout << "start state: ";
	cin >> start;
	string GOAL =  "012345678";
	
	//maximum cost
	int max_h = get_h(start);
		
	deque <Node> Q;
	Node root = Node(start, 0, "");
	int created_nodes= 0;
	Node current;
	Q.push_back(root);
	
	cout << "Searching solution for " << start << endl;
	Node up, left, right, bottom;
	while(!Q.empty()){
		// get next element of queue
		current = Q.front();
		
		
		// find location of 0 in current state as k
		int k;
		for (k=0; k<10; k++){
			if (current.state[k] == '0'){
				break;
			}
		}
		// move up state node
		if (k > 2){
			string up_state = current.state;
			swap(up_state[k], up_state[k-3]);
			up =  Node(up_state, (current.depth)+1, current.path+'U');
			if (up.cost <= max_h){
				Q.push_back(up);
				created_nodes++;
			}
		}
		
		// move left state node

		if (k % 3 > 0){
			string left_state = current.state;
			swap(left_state[k], left_state[k-1]);
			left =  Node(left_state, (current.depth+1), current.path+'L');
			if (left.cost <= max_h){
				Q.push_back(left);
				created_nodes++;
			}
		}
			// move bottom state node
		if (k < 6){
			string bottom_state = current.state;
			swap(bottom_state[k], bottom_state[k+3]);
			bottom = Node(bottom_state, (current.depth)+1,current.path+ 'D');
			if (bottom.cost <= max_h){
				Q.push_back(bottom);
				created_nodes++;
			}
		}
		// move right state node
		if (k % 3 < 2){
			string right_state = current.state;
			swap(right_state[k], right_state[k+1]);
			right =  Node(right_state, (current.depth)+1, current.path+'R');
			if (right.cost <= max_h){
				Q.push_back(right);
				created_nodes++;
			}
		}
		
		system("CLS");
		cout << "Searching solution for " << start << endl;
		cout << "Current depth: " << current.depth <<endl;
		cout << "Generated nodes: " << created_nodes <<endl;
		
		// if its goal break
		if(current.state == GOAL) break;
		Q.pop_front();
		// increase depth by 1 if not found
		if (Q.empty()) {
			Q.push_back(root);
			max_h += 1;
			continue;
		}
		// remove the explored node from queue
		for (int i=0; i<Q.size()-1; i++){
			for (int j=0; j<Q.size()-i-1; j++){
				if (Q[j].cost > Q[j+1].cost){
					Node temp = Q[j];	
					Q[j] =  Q[j+1];
					Q[j+1] = temp;
				} 
			}
		}
	}
	cout << "\n\nFound in depth: " << current.depth << endl;
	cout << created_nodes << " Nodes were created." << endl;
	cout << "PATH: " << current.path;
	return 0;	
}



// compute heuristic using manhatan distance
int get_h(string state){
	int distance = 0;
	for (int k=0; k<9; k++){

		int i = ( state[k]-48 )/3;
		int j = ( state[k]-48 )%3;
		
		int ii = k / 3;
		int jj = k % 3;
		
	
		int dist = abs(i - ii) + abs(j - jj) ;
		distance += dist;
	}
	return distance;
}

